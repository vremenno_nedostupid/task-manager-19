package ru.fedun.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.api.service.IUserService;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.exception.empty.*;
import ru.fedun.tm.exception.incorrect.IncorrectBehaviourException;
import ru.fedun.tm.exception.incorrect.IncorrectPasswordException;
import ru.fedun.tm.exception.notfound.UserNotFoundException;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.util.HashUtil;

import java.util.List;

@AllArgsConstructor
public class UserService implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName,
            @NotNull final String secondName) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPassException();
        if (firstName.isEmpty()) throw new EmptyLoginException();
        if (secondName.isEmpty()) throw new EmptyPassException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setRole(Role.USER);
        return userRepository.add(user);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName,
            @NotNull final String secondName,
            @NotNull final String email
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPassException();
        if (email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = create(login, password, firstName, secondName);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName,
            @NotNull final String secondName,
            @NotNull final Role role
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPassException();
        @NotNull final User user = create(login, password, firstName, secondName);
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @NotNull
    @Override
    public User findById(@NotNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @NotNull
    @Override
    public User updateMail(@NotNull final String userId, @NotNull final String email) {
        if (userId.isEmpty()) throw new AccessDeniedException();
        if (email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = findById(userId);
        user.setEmail(email);
        return user;
    }

    @Override
    public User updatePassword(
            @NotNull final String userId,
            @NotNull final String password,
            @NotNull final String newPassword
    ) {
        if (userId.isEmpty()) throw new AccessDeniedException();
        if (password.isEmpty()) throw new EmptyPassException();
        if (newPassword.isEmpty()) throw new EmptyPassException();
        @NotNull final User user = findById(userId);
        if (!HashUtil.salt(password).equals(user.getPasswordHash())) throw new IncorrectPasswordException();
        user.setPasswordHash(HashUtil.salt(newPassword));
        return user;
    }

    @NotNull
    @Override
    public User removeById(@NotNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @NotNull
    @Override
    public User removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    public User removeUser(@NotNull final String userId, @NotNull final User user) {
        if (userId.isEmpty()) throw new AccessDeniedException();
        return userRepository.remove(user);
    }

    @NotNull
    @Override
    public User lockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(true);
        return user;
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(false);
        return user;
    }

    @Override
    public void load(@NotNull final User... users) {
        userRepository.load(users);
    }

    @Override
    public void load(@NotNull final List<User> users) {
        userRepository.load(users);
    }

}
