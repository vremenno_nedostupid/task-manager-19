package ru.fedun.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.util.TerminalUtil;

public final class LoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login in task manager.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        System.out.println("[OK]");
        System.out.println();
    }

}
