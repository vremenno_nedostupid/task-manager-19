package ru.fedun.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.constant.DataConstant;
import ru.fedun.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public final class DataJsonJaxbClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-clear-json-jb";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear data from json (jax-b) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (JAX-B) CLEAR]");
        @NotNull final File file = new File(DataConstant.FILE_JSON_JB);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles(){
        return new Role[]{Role.ADMIN};
    }

}
