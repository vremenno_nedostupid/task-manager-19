package ru.fedun.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.constant.DataConstant;
import ru.fedun.tm.dto.Domain;
import ru.fedun.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataXmlFasterXmlLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-load-xml-fx";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data to xml (fasterxml) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML (FASTERXML) LOAD]");
        @NotNull final String xmlData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_XML_FX)));
        @NotNull final ObjectMapper xmlMapper = new XmlMapper();
        @NotNull final Domain domain = xmlMapper.readValue(xmlData, Domain.class);
        serviceLocator.getUserService().load(domain.getUsers());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getProjectService().load(domain.getProjects());
        System.out.println("[OK]");
        serviceLocator.getAuthService().logout();
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
