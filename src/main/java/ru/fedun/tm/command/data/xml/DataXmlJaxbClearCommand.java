package ru.fedun.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.constant.DataConstant;
import ru.fedun.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public final class DataXmlJaxbClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-clear-xml-jb";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear data from xml (jax-b) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML (JAX-B) CLEAR]");
        @NotNull final File file = new File(DataConstant.FILE_XML_JB);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles(){
        return new Role[]{Role.ADMIN};
    }

}
