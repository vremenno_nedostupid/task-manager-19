package ru.fedun.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.exception.empty.EmptyDescriptionException;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER TITLE:]");
        @NotNull final String title = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        @NotNull final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, title, description);
        System.out.println("[OK]");
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN, Role.USER };
    }

}
