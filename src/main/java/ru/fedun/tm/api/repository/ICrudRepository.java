package ru.fedun.tm.api.repository;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ICrudRepository<T> {

    @NotNull
    List<T> findAll(@NotNull String userId);

    @NotNull
    List<T> findAll();

    void clear(@NotNull String userId);

    void add(@NotNull String userId, T o);

    void add(@NotNull List<T> o);

    void add(@NotNull T o);

    void remove(@NotNull String userId, T o);

    @NotNull
    T findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    T findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    T findOneByTitle(@NotNull String userId, @NotNull String title);

    @NotNull
    T removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    T removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    T removeOneByTitle(@NotNull String userId, @NotNull String title);

    void load(@NotNull T... o);

    void load(@NotNull List<T> o);

    void clear();

}
