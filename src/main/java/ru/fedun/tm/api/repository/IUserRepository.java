package ru.fedun.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @NotNull
    User add(@NotNull User user);

    @NotNull
    User remove(@NotNull User user);

    @NotNull
    List<User> findAll();

    @NotNull
    User findById(@NotNull String id);

    @NotNull
    User findByLogin(@NotNull String login);

    @NotNull
    User removeById(@NotNull String id);

    @NotNull
    User removeByLogin(@NotNull String login);

    void load(@NotNull User... users);

    void load(@NotNull List<User> users);

    void clear();

}
