package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;

import java.util.List;

public interface IUserService {

    @NotNull
    List<User> findAll();

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @NotNull String firstName,
            @NotNull String lastName);

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @NotNull String firstName,
            @NotNull String lastName,
            @NotNull String email);

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @NotNull String firstName,
            @NotNull String lastName,
            @NotNull Role role);

    @NotNull
    User findById(@NotNull String id);

    @NotNull
    User findByLogin(@NotNull String login);

    @NotNull
    User updateMail(@NotNull String userId, @NotNull String email);

    @NotNull
    User updatePassword(@NotNull String userId, @NotNull String password, @NotNull String newPassword);

    @NotNull
    User removeById(@NotNull String id);

    @NotNull
    User removeByLogin(@NotNull String login);

    @NotNull
    User removeUser(@NotNull String userId, @NotNull User user);

    @NotNull
    User lockUserByLogin(@NotNull String login);

    @NotNull
    User unlockUserByLogin(@NotNull String login);

    void load(@NotNull User... users);

    void load(@NotNull List<User> users);

}
