package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ICrudService<T> {

    void create(@NotNull String userId, @NotNull String title);

    void create(@NotNull String userId, @NotNull String title, @NotNull String description);

    @NotNull
    List<T> findAll(@NotNull String userId);

    @NotNull
    List<T> findAll();

    void clear(@NotNull String userId);

    @NotNull
    T getOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    T getOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    T getOneByTitle(@NotNull String userId, @NotNull String title);

    @NotNull
    T updateById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull String title,
            @NotNull String description
    );

    @NotNull
    T updateByIndex(
            @NotNull String userId,
            @NotNull Integer index,
            @NotNull String title,
            @NotNull String description
    );

    @NotNull
    T removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    T removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    T removeOneByTitle(@NotNull String userId, @NotNull String title);

    void load(@NotNull List<T> o);

    void load(@NotNull T... o);

}
