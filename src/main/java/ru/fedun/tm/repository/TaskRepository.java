package ru.fedun.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.notfound.TaskNotFoundException;
import ru.fedun.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ICrudRepository<Task> {

    @NotNull
    private final List<Task> tasks = new ArrayList<>();

    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @NotNull
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        final List<Task> result = findAll(userId);
        tasks.removeAll(result);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final Task task) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void add(@NotNull final List<Task> tasks) {
        for (@NotNull final Task task : tasks) add(task);
    }

    @Override
    public void add(@NotNull final Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Task task) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        tasks.remove(task);
    }

    @NotNull
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        for (@NotNull final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        throw new TaskNotFoundException();
    }

    @NotNull
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        return tasks.get(index);
    }

    @NotNull
    @Override
    public Task findOneByTitle(@NotNull final String userId, @NotNull final String title) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        for (@NotNull final Task task : tasks) {
            if (title.equals(task.getTitle())) return task;
        }
        throw new TaskNotFoundException();
    }

    @NotNull
    @Override
    public Task removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Task task = findOneByIndex(userId, index);
        remove(userId, task);
        return task;
    }

    @NotNull
    @Override
    public Task removeOneByTitle(@NotNull final String userId, @NotNull final String title) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Task task = findOneByTitle(userId, title);
        remove(userId, task);
        return task;
    }

    @NotNull
    @Override
    public Task removeOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Task task = findOneById(userId, id);
        remove(userId, task);
        return task;
    }

    @Override
    public void load(@NotNull final Task... tasks) {
        clear();
        for (@NotNull final Task task : tasks) add(task);
    }

    @Override
    public void load(@NotNull final List<Task> tasks) {
        clear();
        add(tasks);
    }

    @Override
    public void clear(){
        tasks.clear();
    }

}
