package ru.fedun.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.notfound.ProjectNotFound;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements ICrudRepository<Project> {

    @NotNull
    private final List<Project> projects = new ArrayList<>();

    public void add(@NotNull final String userId, @NotNull final Project project) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        project.setUserId(userId);
        projects.add(project);
    }

    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        projects.remove(project);
    }

    @NotNull
    public List<Project> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @NotNull
    public List<Project> findAll() {
        return projects;
    }

    @SneakyThrows
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<Project> result = findAll(userId);
        projects.removeAll(result);
    }

    @NotNull
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        for (@NotNull final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        throw new ProjectNotFound();
    }

    @NotNull
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        return projects.get(index);
    }

    @NotNull
    @Override
    public Project findOneByTitle(@NotNull final String userId, @NotNull final String title) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        for (final Project project : projects) {
            if (title.equals(project.getTitle())) return project;
        }
        throw new ProjectNotFound();
    }

    @NotNull
    @Override
    public Project removeOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Project project = findOneById(userId, id);
        remove(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Project project = findOneByIndex(userId, index);
        remove(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project removeOneByTitle(@NotNull final String userId, @NotNull final String title) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Project project = findOneByTitle(userId, title);
        remove(userId, project);
        return project;
    }

    @Override
    public void add(@NotNull final List<Project> projects) {
        this.projects.addAll(projects);
    }

    @Override
    public void add(@NotNull final Project project) {
        projects.add(project);
    }

    @Override
    public void load(@NotNull final Project... projects) {
        clear();
        for (@NotNull final Project project : projects) add(project);
    }

    @Override
    public void load(@NotNull final List<Project> projects) {
        clear();
        add(projects);
    }

    @Override
    public void clear() {
        projects.clear();
    }

}
