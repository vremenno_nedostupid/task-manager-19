package ru.fedun.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.exception.notfound.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    @NotNull
    private final List<User> users = new ArrayList<>();

    @NotNull
    @Override
    public User add(@NotNull final User user) {
        users.add(user);
        return user;
    }

    @NotNull
    @Override
    public User remove(@NotNull final User user) {
        users.remove(user);
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return users;
    }

    @NotNull
    @Override
    public User findById(@NotNull final String id) {
        for (@NotNull final User user : users) {
            if (id.equals(user.getId())) {
                return user;
            }
        }
        throw new UserNotFoundException();
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) {
        for (@NotNull final User user : users) {
            if (login.equals(user.getLogin())) {
                return user;
            }
        }
        throw new UserNotFoundException();
    }

    @NotNull
    @Override
    public User removeById(@NotNull final String id) {
        @NotNull final User user = findById(id);
        remove(user);
        return user;
    }

    @NotNull
    @Override
    public User removeByLogin(@NotNull final String login) {
        @NotNull final User user = findByLogin(login);
        remove(user);
        return user;
    }

    @Override
    public void load(@NotNull final User... users) {
        clear();
        for (@NotNull final User user : users) add(user);
    }

    public void load(@NotNull final List<User> users) {
        clear();
        for (@NotNull final User user : users) add(user);
    }

    @Override
    public void clear() {
        users.clear();
    }

}
